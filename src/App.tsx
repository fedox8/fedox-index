import React from 'react';
import Console from "./components/Console";
import './scss/main.scss'

const App: React.FC = () => {
    return (
        <div className="section">
            <div className="container">
                <div className="level is-mobile is-not-selectable">
                    <div className="level-left">
                        <h1 className="title is-1 page-title">Fedox</h1>
                    </div>
                    <div className="level-right">
                        <p>This site uses <a target="_blank" rel="noopener noreferrer" href="https://en.wikipedia.org/wiki/HTTP_cookie">cookies</a></p>
                    </div>
                </div>
                <hr/>
                <div className="console">
                    <p>Fedox Bash
                        [Version 1.0]</p>
                    <p>&copy; 2019 Fedox. All rights reserved.</p>
                    <br/>
                    <p>Enter a command or type "help" for help.</p>
                    <Console/>
                </div>
            </div>
        </div>
    );
};

export default App;
