// @ts-ignore
import bashEmulator from "bash-emulator"

export default class Bash {
    private emulator: any;
    public static help: string = "Fedox Bash, version 1.0\n" +
        "These shell commands are defined internally\n" +
        "Type help to see this list\n\n" +
        "cd    - Change the shell working directory.\n" +
        "ls    - list directory contents\n"+
        "pwd   - print name of current/working directory\n" +
        "cat   - concatenate files and print on the standard output\n" +
        "mkdir - make directories\n" +
        "mv    - move (rename) files\n" +
        "cp    - copy files and directories\n" +
        "rm    - remove files or directories\n" +
        "rmdir - remove empty directories\n" +
        "clear - clears your screen if this is possible";

        constructor(emulatorState: any) {
        this.emulator = bashEmulator(emulatorState);
        this.emulator.commands.help = function (env: any, args:any) {
            env.output(Bash.help);
            env.exit(0);
        }
    }


    public run(command: string):string {
        console.log(`Command run: ${command}`);
        return this.emulator.run(command)
            .then((result: string) => {
                console.log(result);
                return result;
            })
            .catch((error: string) => {
                console.log(error);
                return error.toString();
            });
    }

    public getDir(){
        return this.emulator.getDir().then((result: string)=>{
            return result;
        })
    }



}
