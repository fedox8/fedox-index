export interface ConsoleState {
    command: string,
    path: string,
    history: Array<string>;
}
