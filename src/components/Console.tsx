import React, {Component, createRef} from "react";
import '../scss/main.scss'
import {ConsoleState} from "./interfaces";
import Bash from "../bash";

export default class Console extends Component {
    public state: ConsoleState;
    private bash: Bash;
    private readonly ref: any;

    constructor(props: any) {
        super(props);
        this.state = {
            command: '',
            path: '/',
            history: [],
        };
        this.bash = new Bash(
            {
                user: 'guest',
                workingDirectory: '/',
                fileSystem: {
                    '/': {
                        type: 'dir',
                        modified: new Date(1567881830612),
                    },
                    '/About-Me': {
                        type: 'dir',
                        modified: new Date(1567881830612),
                    },
                    '/About-Me/EN.txt': {
                        type: 'file',
                        modified: new Date(1567884659745),
                        content: `I am a young programmer, I focus mainly on creating web application's backend. I love to build apps using google cloud and firebase.\n\n` +
                            `Language Skills:\n` +
                            `Great: Typescript, Javascript, EJS, HTML, CSS, Sass\n` +
                            `Good: Python, C#, Bash\n` +
                            `Currently Learning: Dart\n` +
                            `PHP: I know it well, but I don't like to use it\n` +
                            `\n` +
                            `DEV Tools:\n` +
                            `IDE: Webstorm, Rider, PyCharm, InteliJ, PhpStorm\n` +
                            `Version Control: git\n` +
                            `Repository hosting: Bitbucket, Github`
                    },
                    '/About-Me/PL.txt': {
                        type: 'file',
                        modified: new Date(1567884659745),
                        content: `Jestem młodym programistą, koncentruję się głównie na tworzeniu backendu aplikacji webowych. Uwielbiam budować w oparciu o google cloud i firebase.\n\n` +
                            `W czym mówię:\n` +
                            `Bardzo dobrze: Typescript, Javascript, EJS, HTML, CSS, Sass\n` +
                            `Dobrze: Python, C#, Bash\n` +
                            `Uczę się: Dart\n` +
                            `PHP: Znam całkiem nieźle, ale nie lubię używać\n` +
                            `\n` +
                            `Narzędzia, z których korzystam:\n` +
                            `IDE: Webstorm, Rider, PyCharm, InteliJ, PhpStorm\n` +
                            `Kontrola Wersji: git\n` +
                            `Hostowanie Kodu: Bitbucket, Github`
                    },
                    '/contact.txt': {
                        type: 'file',
                        modified: new Date(1567881830612),
                        content: 'email:\x20fedox@fedox.pl\x20\x0adiscord:Fedox#1886\x20\x0afacebook:\x20mmbogus\x20\x0aphone:\x20+48\x20513\x20322\x20198',
                    },
                    '/home': {
                        type: 'dir',
                        modified: new Date(1535234400000),
                    },
                    '/home/my-flower.jpg': {
                        type: 'file',
                        modified: new Date(1535234400000),
                        content: 'https://fedox.pl/i/i4HaEburcd4PnVBw'
                    },
                    '/home/favourite-movies.txt': {
                        type: 'file',
                        modified: new Date(1567965720417),
                        content: 'My favorite movies:\n' +
                            '1. Matrix\n' +
                            '2. Shawshank Redemption\n' +
                            '3. The Godfather\n' +
                            '4. Vabank\n' +
                            '5. Gone With The Wind\n' +
                            '6. Silence of the Lambs\n' +
                            '7. 12 Angry Men\n' +
                            '8. Pulp Fiction\n' +
                            '9. One Flew Over the Cuckoo\'s Nest\n' +
                            '10. The Favourite',
                    },
                    '/home/favourite-books.txt': {
                        type: 'file',
                        modified: new Date(1567969583697),
                        content: 'My favorite movies:\n' +
                            '1. George Orwell - 1984\n' +
                            '2. Solaris - Stanisław Lem\n' +
                            '3. Harry Potter and the Order of the Phoenix - J. K. Rowling\n' +
                            '4. A Brief History of Time - Stephen Hawking\n' +
                            '5. Szachista - Waldemar Łysiak\n' +
                            '6. Astrophysics for People in a Hurry - Neil deGrasse Tyson\n' +
                            '7. The Godfather - Mario Puzo\n' +
                            '8. Death Train - Alastair MacNeill\n' +
                            '9. Officer factory - Hans Hellmut Kirst',
                    },
                }
            });
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.ref = createRef();
    }

    private renderHistory() {
        function html(input: string): { __html: string } {
            input = input.replace(/\n/g, '<br/>');
            return {__html: input};
        }

        setTimeout(() => {
            this.ref.current.scrollIntoView({
                behavior: 'smooth',
                inline: 'nearest',
            })
        }, 10);

        return this.state.history.map((command, index) => {
            return (<div key={index} className="cli" dangerouslySetInnerHTML={html(command)}/>);
        });
    }

    private handleChange(event: any) {
        this.setState({command: event.target.value});
    }

    private async handleKeyUp(event: any) {
        if (event.keyCode === 13) {
            if (this.state.command === 'clear') {
                let path = await this.bash.getDir();
                this.setState({
                    command: '',
                    path,
                    history: [],
                })
            } else {
                let history = this.state.history.concat(`[guest@fedox ${this.state.path}]$&nbsp;${this.state.command}`);
                history = history.concat(await this.bash.run(this.state.command));
                let path = await this.bash.getDir();
                this.setState({
                    command: '',
                    path,
                    history,
                });
            }
        }
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div id="console">
                <div id="history">
                    {this.renderHistory()}
                </div>
                <div className="columns is-gapless is-mobile">
                    <div className="cli-input column is-narrow">
                        [guest@fedox {this.state.path}]$&nbsp;
                    </div>
                    <div className="column cli-input">
                        <input
                            onKeyUp={this.handleKeyUp}
                            autoCapitalize="off"
                            ref={this.ref}
                            onChange={this.handleChange}
                            value={this.state.command}
                            className="cmd-input"
                            type="text"
                            id="0"
                            autoComplete="off"
                            autoFocus/>
                    </div>
                </div>
            </div>
        );
    }
}
